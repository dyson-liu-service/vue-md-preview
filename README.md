# Markdown Editor Developed with VUE3 and Vuetify

This document will walk you through the process of setting up a Vue3 + Vite project within the Dev Container environment in VS Code. We'll illustrate this by creating a small program that converts Markdown text into an HTML interface. This guide is designed to give readers hands-on experience in Vue3 programming, as well as to showcase the convenience offered by the Dev Container.

> - This document and the related code are stored on GitLab: [vue-md-preview](https://gitlab.com/dyson-liu-service/vue-md-preview)  
> - The demonstration program is deployed on GitLab Pages: [DEMO](https://vue-md-preview-dyson-liu-service-297d0f12f9980bfdfc70bed7546ae2.gitlab.io/)


- [Markdown Editor Developed with VUE3 and Vuetify](#markdown-editor-developed-with-vue3-and-vuetify)
  - [Setting Up a Dev Container for Vue3 Development](#setting-up-a-dev-container-for-vue3-development)
  - [Create Project with Vuetify](#create-project-with-vuetify)
  - [Create Project with Vue3](#create-project-with-vue3)
    - [Modify main.ts to Include Vuetify Packages](#modify-maints-to-include-vuetify-packages)
  - [Project Folder/Files Structure](#project-folderfiles-structure)
  - [Using `marked` to Convert markdown Text to HTML](#using-marked-to-convert-markdown-text-to-html)
  - [Simple Markdown Editor and Preview Component](#simple-markdown-editor-and-preview-component)
  - [Reference](#reference)

Utilizing Vue3, Vuetify, and marked to develop a simple Markdown editor that can instantly display the results of editing.

## Setting Up a Dev Container for Vue3 Development

- In VS Code, press `F1`
- Choose 'Dev Containers: Open Folder in Container...'
- Select: 'Vue community' (Develop an application with Vue.js, includes everything you need to get up and running.)
- Choose Node.js version: 18
- Select additional features to install: (skip)
- Then waiting "Adding Dev Container Configuration Files.." for couple menutes.

## Create Project with Vuetify

The simplest way to set up a Vue3 + Vuetify3 project is to execute the 'create vuetify' command:

```bash
yarn create vuetify
# Project name: vue-md-preview
# ✔ Which present would you like ti inatll?
# ✔ > Default (Vuetify)
#     Base (Vuetify, VueRouter)
#     Essentials (Vuetify, VueRouter, Pinia)
#     Custom (Choose your features)
# ✔ Use TypeScript? Yes
# ✔ Would you like to install dependencies with yarn, npm, or pnpm? yarn

cd vue-md-preview
yarn dev
```

If you encounter a vite version error, you can adjust it with the following command:

```bash
yarn add @vitejs/plugin-vue@latest
```

## Create Project with Vue3

You can also add Vuetify3 after setting up the Vue3 project. The steps are as follows:

- Create Vue3 project:
    ```bash
    npm init vue@3.6.4
    # Need to install the following packages:
    #   create-vue@3.6.4
    # Ok to proceed? (y) y
    # ✔ Project name: vue-md-preview
    # ✔ Add TypeScript? › Yes
    # ✔ Add JSX Support? › No
    # ✔ Add Vue Router for Single Page Application development? › No
    # ✔ Add Pinia for state management? › No
    # ✔ Add Vitest for Unit Testing? › No
    # ✔ Add an End-to-End Testing Solution? › No
    # ✔ Add ESLint for code quality? › No
    #
    # Scaffolding project in /workspaces/vue-editor/vue-md-preview...
    #
    # Done. Now run:
    #
    cd vue-md-preview
    npm install
    npm install vuetify@^3.3.0
    npm run dev
    ```

### Modify main.ts to Include Vuetify Packages

Modify the main.ts program to import relevant Vuetify packages. Here's an example:

```ts
import { createApp } from 'vue'
import App from './App.vue'

// Vuetify 
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const vuetify = createVuetify({
  components,
  directives,
})

createApp(App).use(vuetify).mount('#app')
```

## Project Folder/Files Structure 

The folder and file structure of the vue-md-preview project created by Vuetify is generally as follows. In this project, we modified the content in /src/index.ts and /src/App.vue, adjusted the function registerPlugins in /plugins/index.ts to the function chaining form, and added two components, MarkdownEditor.vue and MarkdownSample.vue.

```
/vue-md-preview
├── node_modules/
├── public/
│   ├── favicon.ico
│   └── index.html
├── src/
│   ├── assets/
│   ├── components/
│   │   ├── MarkdownEditor.vue (add)
│   │   └── MarkdownSample.vue (add)
│   ├── plugins/
│   │   ├── index.ts (modify)
│   │   ├── vuetify.ts
│   │   └── webfontloader.ts
│   ├── App.vue (modify)
│   ├── main.js (modify)
│   └── index.css
├── .gitignore
├── package.json
├── package-lock.json
├── README.md
└── ...
```

## Using `marked` to Convert markdown Text to HTML

markedjs/marked can convert markdown text into HTML for display. The steps are as follows:

```bash
yarn add marked
yarn add @types/marked --dev
or 
npm install marked
npm install @types/marked --save-dev # For TypeScript projects
```

## Simple Markdown Editor and Preview Component

The following MarkdownEditor.vue uses the Vue3's computed() function to instantly convert the text entered in the textarea into HTML for display. It also utilizes packages such as vuetify3 and marked.

```html
<template>
  <v-container fluid>
    <v-row>
      <v-col cols="6">
        <v-card 
          title="Markdown Editor"  
          subtitle="Please edit Markdown syntax:">
          <v-divider></v-divider>
          <v-card-item>
            <v-textarea 
              outlined
              variant="outlined" 
              auto-grow
              no-resize
              v-model="inputMarkdown" />
          </v-card-item>
        </v-card>
      </v-col>
      <v-col cols="6">
        <v-card 
          title="Markdown Preview"
          subtitle="Below is the Markdown preview:">
          <v-divider></v-divider>
          <v-card-item>
            <div v-html="compiledMarkdown"></div>
          </v-card-item>          
        </v-card>        
      </v-col>
    </v-row>
  </v-container>
</template>

<style scoped>
</style>

<script setup lang="ts">

import { ref, computed } from "vue";
import { marked } from "marked";
import MarkdownSample from "./MarkdownSample.vue";

const inputMarkdown = ref(MarkdownSample.sampleText);
const compiledMarkdown = computed(() => marked.parse(inputMarkdown.value));

</script>
```

## Reference
- [Vuetify 3](https://vuetifyjs.com/en/)
- [markedjs/marked on Github](https://github.com/markedjs/marked)
- [Marked Documentation](https://marked.js.org/)
- [CORRECTLY setting up VSCode + TypeScript + VueJS 3 + EsLint + Prettier](https://gist.github.com/armenr/4bb551c387b462423612fbc2ac68905b)
- [Material Design Icons](https://pictogrammers.com/library/mdi/)