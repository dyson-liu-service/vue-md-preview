/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

import App from './App.vue'         // Components
import { createApp } from 'vue'     // Composables
import { registerPlugins } from '@/plugins' // Plugins

createApp(App).use(registerPlugins).mount('#app')
